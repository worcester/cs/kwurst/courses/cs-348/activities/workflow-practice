# Workflow Practice Activity

## Content Learning Objectives

By the end of this activity, participants will be able to...

- 

## Process Skill Goals

During the activity, students should make progress toward:

- 

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1 - Claiming an Issue and Creating a Merge Request

### Each team member will do these steps individually. Ask for help from your teammates if necessary:

1. Go to the issue list, open the issue you have been assigned by your team, and assign it to yourself.
2. Click the `Create merge request` button to create a feature branch, and a merge request.
    1. Notice the branch name and which branch it will be merged into. Make sure that makes sense to you.
    2. Look at the title of the merge request. Notice that `Draft:` has been added, and the note below the title.
    3. Read the description. Discuss with your team. Is there anything that you don't understand?
    4. Remove the `Closes` line from the description. Our workflow will be to move issues to `Done` column of your board when complete. We will close issues during the Sprint Review.
3. Click `Create merge request`
4. Move your issue to the `In Process` column of your board.

## Model 2 - Working on your Issue

### Each team member will do these steps individually. Ask for help from your teammates if necessary:

1. Clone your team's project repository to your computer.
2. Open the repository in Visual Studio Code.
3. Switch to the branch associated with your issue and merge request.

    ```bash
    git switch <branch-name>
    ```

    Your branch name will start with the the issue number. After typing `git switch`, type the issue number and hit `<Tab>` to use completion.

4. Nothing will have changed in this branch yet, but it's good practice to do `git pull` to make sure there are no remote changes.
5. Make the changes needed to complete the issue.

    - Make sure you add, commit, and push your changes.

6. Follow the link to your merge request that was shown in the `git push` message.

    - Look at the `commits` tab to see your commits.
    - Look at the `changes` tab to see the changes to the file(s).
    - Look at the `overview` tab.

7. Mark your merge request as ready for review (removing DRAFT:)
8. Move your issue to the `Needs Review` column of your board.

## Model 3 - Reviewing a Merge Request

### Each team member will do these steps individually. Ask for help from your teammates if necessary:

1. Pick one of your teammates' Merge Requests to review. Assign yourself as the reviewer.
2. Review their merge request.

    - Based on the issue's description, and your team's Definition of Done, determine if the merge request satisfies the issue.
    - If not, suggest changes in the comments. Make sure to mention your teammate with an `@`
    - If yes, then go ahead and merge!
        - Make sure `Delete source branch` is checked.
        - Click the `Merge` button.
        - Check `Merge Requests` to see that it has been merged.
        - Check `Repository>Branches` to see that the issue branch has been deleted.
        - Move the issue to the `Done` column of your board.

## Model 4 - Continue until done

### Each team member will do these steps individually. Ask for help from your teammates if necessary:

- If your reviewing teammate requested changes, repeat Models 2 and 3 until it meets their requirements. Have a conversation with them to make sure you are both on the same page about the requirements.
- If your changes were merged, you can delete your local branch:

    ```bash
    git switch main
    git branch -D <branch-name>
    ```
- If there are still unassigned issues, claim another one and repeat the process.

## Model 5 - Sprint Review

When all the issues are in the `Done` column, as a team look through the issues in the `Done` column and discuss them. Close the issues as you do so.

&copy; 2023 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA